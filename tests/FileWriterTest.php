<?php 

class FileWriterTest extends PHPUnit_Framework_TestCase
{
    protected $fw;

    public function setUp()
    {
        $this->fw = new FileWriter;
    }
    public function test_it_works()
    {
        $this->assertInstanceOf('FileWriter', $this->fw);
    }

    public function test_it_can_parse_an_array_into_json()
    {
        $input = [
            'fruits' => ['apples', 'oranges']
        ];

        // what we are expecting back!
        $output = '{ "fruits": ["apples", "oranges"] }';

        $this->assertEquals($output, $this->fw->toJson($input));
    }
} 